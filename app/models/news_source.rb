class NewsSource < ApplicationRecord
  resourcify
  attr_reader :category_tokens
  has_and_belongs_to_many :news_categories,
    :join_table => :news_categories_news_sources

  has_many :org_ratings, dependent: :delete_all
  has_many :orgs, through: :org_ratings, dependent: :delete_all

  geocoded_by :full_address
  after_validation :geocode, if: ->(obj){ obj.full_address.present? and obj.full_address_changed? }

  def category_tokens=(tokens)
    self.news_category_ids = NewsCategory.ids_from_tokens(tokens)
  end
end
