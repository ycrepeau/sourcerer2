class Ability
  include CanCan::Ability

  def initialize(user)
    # Define abilities for the passed in user here. For example:
    #
    user ||= User.new # guest user (not logged in)

    if user.has_role? :admin
      can :access, :rails_admin       # only allow admin users to access Rails Admin
      can :dashboard                  # allow access to dashboard

      can :manage,  :all
    elsif user.has_role? :editor
      can :create,  NewsSource
      can :read,    NewsSource
      can :update,  NewsSource
      # but :destroy is missing

      can :read,    Membership
      can :read,    Org

      # A manager can update an Org if he/she is member of this Org
      can :update,  Org, :users => { :id => user.id }

      # A member can update membership only if his/her credibility
      # is larger of a certain treshold. The membships.admin returns
      # only members with credibility higher than 800
      can :registrar, Org do |org|
        org.memberships.admin.ids.include? user.id
      end
    else
      can :read, :all
      cannot :read, Membership
    end


    #if user.admin?
    #  can :manage, :all
    #else
    #  can :read, :all
    #end
    #
    # The first argument to `can` is the action you are giving the user
    # permission to do.
    # If you pass :manage it will apply to every action. Other common actions
    # here are :read, :create, :update and :destroy.
    #
    # The second argument is the resource the user can perform the action on.
    # If you pass :all it will apply to every resource. Otherwise pass a Ruby
    # class of the resource.
    #
    # The third argument is an optional hash of conditions to further filter the
    # objects.
    # For example, here the user can only update published articles.
    #
    #   can :update, Article, :published => true
    #
    # See the wiki for details:
    # https://github.com/CanCanCommunity/cancancan/wiki/Defining-Abilities
  end
end
