class MesTruc < ApplicationRecord
  resourcify
  validates :title, presence: true
  validates :artist, presence: true
end
