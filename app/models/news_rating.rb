class NewsRating < ApplicationRecord
  belongs_to :user
  belongs_to :org
  belongs_to :news_source
end
