json.extract! news_rating, :id, :news_source_id,  :org, :user_id, :rating
json.url news_rating_url(news_rating, format: :json)
