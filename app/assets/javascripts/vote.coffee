addTumbsLine = (p, value) ->
  #constants
  D0 = $("<span class='fa fa-lg fa-thumbs-o-down'></span>")
  D1 = $("<span class='fa fa-lg fa-thumbs-down'></span>")
  U0 = $("<span class='fa fa-lg fa-thumbs-o-up'></span>")
  U1 = $("<span class='fa fa-lg fa-thumbs-up'></span>")
  N = $("<span class='fa fa-lg fa-circle'></span>")

  p.empty()
  p.removeClass('p')
  p.removeClass('o')
  p.removeClass('n')
  if value == 0
    p.addClass('o')
    [1..5].forEach -> D0.clone().appendTo(p)
    N.appendTo(p)
    [1..5].forEach -> U0.clone().appendTo(p)
  else if value < 0 && value > -5
    p.addClass('n')
    [-4..value].forEach -> D0.clone().appendTo(p)
    [value..-1].forEach -> D1.clone().appendTo(p)
    N.appendTo(p)
    [1..5].forEach -> U0.clone().appendTo(p)
  else if value < 0 && value = -5
    p.addClass('n')
    [-5..-1].forEach -> D1.clone().appendTo(p)
    N.appendTo(p)
    [1..5].forEach -> U0.clone().appendTo(p)
  else if value > 0 && value < 5
    p.addClass('p')
    [1..5].forEach -> D0.clone().appendTo(p)
    N.appendTo(p)
    [1..value].forEach -> U1.clone().appendTo(p)
    [value..4].forEach -> U0.clone().appendTo(p)
  else if value > 0 && value = 5
    p.addClass('p')
    [1..5].forEach -> D0.clone().appendTo(p)
    N.appendTo(p)
    [1..5].forEach -> U1.clone().appendTo(p)

addVoteSection = (orgId, orgName, sourceid, value, updateUrl) ->

  #variables from parameters and others
  divVote = $('#divVote')
  p = $("<p data-sourceid=#{sourceid} data-orgid=#{orgId} data-updateurl=#{updateUrl}></p>").appendTo(divVote)
  p.before("<h5>#{orgName}</h5>")
  addTumbsLine(p,value)




  #Add a click handler when the user clicks
  p.delegate 'span', 'click', ->
    vote = $(this).index() - 5
    sourceid = $(this).parent().attr('data-sourceid')
    parent = $(this).parent()
    orgid = parent.attr('data-orgid')
    updateurl = parent.attr('data-updateurl')
    console.log("Vote: #{vote} sourceid: #{sourceid} orgid: #{orgid} update: #{updateurl}")
    addTumbsLine(parent,vote)
    $.ajax
      url: updateurl
      type: 'PATCH'
      data:
        "news_rating[rating]": vote
      error: (jqXHR, extStatus, errorThrown ) ->
        console.log("Error raised for #{updateurl}")
        console.log("Error: #{errorThrown}")
      success: (data, textStatus, jqXHR ) ->
        console.log("Success for #{updateurl}")
setup = ->
  console.log 'vote system setup'


  $('a[data-toggle="tab"]').on 'shown.bs.tab', (e) ->
    target = $(e.target).attr("href")
    if target == '#divVote'
      sourceid = $(target).attr('data-sourceid')
      xurl = $(target).attr('data-xurl')
      sourceurl =  xurl + '?source_id=' + sourceid
      $('#divVote').empty()
      #addVoteSection(2,'Personnal', sourceid,-5)
      console.log "Calling ajax with: #{sourceurl}"
      $.get sourceurl, (data) ->
        for item in data
          if (item.org != null)
            addVoteSection(item.org.id,item.org.name, sourceid,item.rating, item.url)
          else
            addVoteSection(0,"Personnal", sourceid,item.rating, item.url)


$ ->
  console.log("Loading page vote.coffee...!  " + Date.now()%10000)
  setup()
  document.addEventListener "turbolinks:load", () ->
    console.log('turbolink:load in vote.coffee')
    setup()
