setup = ->
  console.log("news_sources.coffee setup")
  $('#news_source_category_tokens').tokenInput('/news_categories.json', {
    theme: 'facebook'
    prePopulate: $('#news_source_category_tokens').data('load')
  })

jQuery ->
  console.log('Loading page news_sources.coffee')
  setup()

  document.addEventListener "turbolinks:load", () ->
    console.log('turbolink:load in news_sources.coffee')
    setup()
