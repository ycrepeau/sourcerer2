# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20161214182411) do

  create_table "memberships", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "org_id"
    t.integer  "credibility"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.index ["org_id"], name: "index_memberships_on_org_id"
    t.index ["user_id"], name: "index_memberships_on_user_id"
  end

  create_table "mes_trucs", force: :cascade do |t|
    t.string   "title"
    t.string   "artist"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "news_categories", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "news_categories_news_sources", id: false, force: :cascade do |t|
    t.integer "news_source_id"
    t.integer "news_category_id"
    t.index ["news_source_id", "news_category_id"], name: "index_news_sources_news_categoies_on_source_and_category"
  end

  create_table "news_ratings", force: :cascade do |t|
    t.integer  "news_source_id"
    t.integer  "org_id"
    t.integer  "user_id"
    t.integer  "rating"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
    t.index ["news_source_id"], name: "index_news_ratings_on_news_source_id"
    t.index ["org_id"], name: "index_news_ratings_on_org_id"
    t.index ["user_id"], name: "index_news_ratings_on_user_id"
  end

  create_table "news_sources", force: :cascade do |t|
    t.string   "name"
    t.string   "url"
    t.text     "description"
    t.string   "full_address"
    t.float    "longitude"
    t.float    "latitude"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
    t.string   "lang"
  end

  create_table "org_descriptions", force: :cascade do |t|
    t.integer  "org_id"
    t.string   "lang",        limit: 8
    t.text     "description", limit: 2000
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
    t.index ["org_id", "lang"], name: "org_lang_index_for_org_descriptions"
  end

  create_table "org_ratings", force: :cascade do |t|
    t.integer  "news_source_id"
    t.integer  "org_id"
    t.integer  "rating"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
    t.index ["news_source_id"], name: "index_org_ratings_on_news_source_id"
    t.index ["org_id"], name: "index_org_ratings_on_org_id"
  end

  create_table "orgs", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "roles", force: :cascade do |t|
    t.string   "name"
    t.string   "resource_type"
    t.integer  "resource_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.index ["name", "resource_type", "resource_id"], name: "index_roles_on_name_and_resource_type_and_resource_id"
    t.index ["name"], name: "index_roles_on_name"
  end

  create_table "t1s", force: :cascade do |t|
    t.string   "name"
    t.date     "birth_date"
    t.integer  "score"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "users", force: :cascade do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.boolean  "admin"
    t.string   "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string   "unconfirmed_email"
    t.index ["confirmation_token"], name: "index_users_on_confirmation_token", unique: true
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

  create_table "users_roles", id: false, force: :cascade do |t|
    t.integer "user_id"
    t.integer "role_id"
    t.index ["user_id", "role_id"], name: "index_users_roles_on_user_id_and_role_id"
  end

end
