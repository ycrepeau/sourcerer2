class CreateNewsSources < ActiveRecord::Migration[5.0]
  def change
    drop_table :news_sources if ActiveRecord::Base.connection.table_exists? :news_sources
    create_table :news_sources do |t|
      t.string :name
      t.string :url
      t.text :description
      t.string :full_address
      t.float :longitude
      t.float :latitude

      t.timestamps
    end
  end
end
