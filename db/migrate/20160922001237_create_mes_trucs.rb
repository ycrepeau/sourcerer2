class CreateMesTrucs < ActiveRecord::Migration[5.0]
  def change
    create_table :mes_trucs do |t|
      t.string :title
      t.string :artist

      t.timestamps
    end
  end
end
