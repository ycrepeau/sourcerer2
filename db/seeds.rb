# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
NewsSource.delete_all

NewsSource.create(
  name:'CNN',
  url:'http://rss.cnn.com/rss/cnn_topstories.rss',
  description:'US based cable news network',
  full_address:'190 Marietta St, Atlanta, GA 30303, US',
  lang:'en'
)


NewsSource.create(
  name:'Houston Chronicle',
  url:'http://www.chron.com/rss/feed/News-270.php',
  description:'Texax daily newspaper',
  full_address:'747 Southwest Fwy, Houston, TX 77027, US',
  lang:'en'
)


NewsSource.create(
  name:'The Guardian',
  url:'https://www.theguardian.com/uk/rss',
  description:'UK daily newspaper',
  full_address:'Kings Place, 90 York Way, Kings Cross, London N1 9GU, UK',
  lang:'en'
)

NewsSource.create(
  name:'Montreal Gazette',
  url:'http://montrealgazette.com/feed/',
  description:'Montreal Daily newspaer',
  full_address:'1010 Rue Sainte-Catherine, Montréal, QC H3B 5L1, CA',
  lang:'en'
)


NewsSource.create(
  name:'The New York Times',
  url:'http://rss.nytimes.com/services/xml/rss/nyt/InternationalHome.xml',
  description:'US (New York) daily newspaper',
  full_address:'620 8th Ave, New York, NY 10018, US',
  lang:'en'
)

NewsSource.create(
  name:'Toronto Sun',
  url:'http://www.torontosun.com/home/rss.xml',
  description:'Toronto based tabloid daily',
  full_address:'365 Bloor St E, Toronto, ON M4W 3S9. CA',
  lang:'en'
)

NewsSource.create(
  name:'The Daily Star',
  url:'http://www.thedailystar.net/frontpage/rss.xml',
  description:'Dahka based newspaper',
  full_address:'64-65, Kazi Nazrul Islam Ave, Dhaka 1215, BD',
  lang:'en'
)

NewsSource.create(
  name: 'Time of India',
  url: 'http://timesofindia.indiatimes.com/rssfeedstopstories.cms',
  description: "Time of India, an old daily newspaper",
  full_address: 'Ground Floor,Trade Avenue Building,Suren Road, Off Western Express Highway,Andheri East, J B Nagar, Andheri East, Mumbai, Maharashtra 400093, IN',
  lang:'en'
)

NewsSource.create(
  name: 'Bild',
  url:'http://www.bild.de/rssfeeds/vw-home/vw-home-16725562,sort=1,view=rss2.bild.xml',
  description:'largest Germand daily',
  full_address: 'Volgersweg 2-3, 30175 Hannover, DE',
  lang:'de'
)

NewsSource.create(
  name: 'दैनिक भास्कर (Dainik Bhaskar )',
  url: 'http://www.bhaskar.com/rssfeedtopnews',
  description:'Largest printed daily newspaper in India',
  full_address: '6, Ram Gopal Maheshwari Marg, Indira Press Complex, Zone-I, Maharana Pratap Nagar, Bhopal, Madhya Pradesh 462011, IN',
  lang:'hi'
)

NewsSource.create(
  name: '読売新聞 Yomiuri Shinbun',
  url: 'http://www.yomiuri.co.jp/',
  description:'Largest printed daily in the World',
  full_address:'Japan, 〒100-8055 Tokyo, 千代田区Otemachi, 1−7−1, JP',
  lang:'jp'
)

NewsSource.create(
  name: 'Независимая газета Nezavisimaya Gazeta',
  url: 'http://www.ng.ru/rss/',
  description: 'Russian daily',
  full_address: 'Myasnitskaya ul., 13, стр. 3, Moskva, Russie, 101000, RU',
  lang:'ru',
)

NewsSource.create(
  name: 'Jerusalem Post',
  url: 'http://www.jpost.com/Rss/RssFeedsHeadlines.aspx',
  description: 'Israeli daily',
  full_address: 'האחים מסלאויטה 13, תל אביב יפו, 6701024, ישראל, IL',
  lang:'en',
)


<<End

NewsSource.create(
  name: '',
  url: '',
  description: '',
  full_address: '',
  lang:'en',
)

End
